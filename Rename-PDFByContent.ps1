Unblock-File -Path .\lib\iTextSharp.dll
Add-Type -Path .\lib\itextsharp.dll

function Get-UIDFromPdf
{
    [CmdletBinding()]
    param (
        [Parameter(Mandatory = $true)]
        [string]
        $Path,
        [Parameter(Mandatory = $true)]
        [string]
        $SearchString
    )

    $Path = $PSCmdlet.GetUnresolvedProviderPathFromPSPath($Path)

    $reader = New-Object iTextSharp.text.pdf.pdfreader -ArgumentList $Path

    $lines = [iTextSharp.text.pdf.parser.PdfTextExtractor]::GetTextFromPage($reader, 1) -split "\r\n"

    $output = ($lines | Where-Object { $_ -like ('*' + $SearchString + '*') }).Trim()

    $reader.Close()

    return $output
}



$files = Get-ChildItem -Filter '*.pdf' -File

$files | ForEach-Object {

    $uid = Get-UIDFromPdf -Path $_.FullName -SearchString 'Student Id:'
    $newname = $uid.Replace('Student Id:','').Trim()

    if($uid.Length -eq 7){

        $_ | Rename-Item -NewName "$newname.pdf"

    }

}